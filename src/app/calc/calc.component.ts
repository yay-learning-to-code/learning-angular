import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calc',
  templateUrl: './calc.component.html',
  styleUrls: ['./calc.component.scss'],
})
export class CalcComponent implements OnInit {
  input: string = '';
  result: string = '';

  pressNumber(number: string) {
    this.input += number;
  }

  private isLastValueOperand(): boolean {
    const conditions = ['+', '-', '/', '*'];
    let lastChar = this.input[this.input.length - 1];
    console.log('Last char: ' + lastChar);

    let returnValue = false;

    conditions.forEach((operand) => {
      if (operand === lastChar) {
        returnValue = true;
      }
    });

    return returnValue;
  }

  pressOperator(operator: string) {
    // check if an operator is already in the equasion.
    // If so, we want to calculate that result first and add this operator to the equasion afterwards.
    const conditions = ['+', '-', '/', '*'];

    if (this.isLastValueOperand()) {
      console.log('The last char was an operand.');
      this.input = this.input.substring(0, this.input.length - 1) + operator;
    } else if (conditions.some((val) => this.input.includes(val))) {
      this.getAnswer();
      this.input = this.result + operator;
    } else {
      this.input += operator;
    }
  }

  allClear() {
    this.input = '';
  }

  clearLast() {
    if (this.input != '') {
      this.input = this.input.substring(0, this.input.length - 1);
    }
  }

  getAnswer() {
    if (this.input != '') {
      this.result = this.calcAnswer(this.input);
    }
  }

  private calcAnswer(equasion: string): string {
    let answer = NaN;

    let operator = equasion.match('[+-/\\*]');
    if (operator != null || operator != undefined) {
      let op = operator[0];
      console.log('Doing this operation: ' + op);

      let values = equasion.split(op);

      values.forEach((value) => {
        console.log('===> value: ' + value);
        if (isNaN(answer)) {
          console.log('First value: ' + value);
          answer = Number(value);
        } else {
          console.log('Post-First value:' + value);
          switch (op) {
            case '+':
              answer += Number(value);
              break;
            case '-':
              answer -= Number(value);
              break;
            case '*':
              answer = answer * Number(value);
              break;
            case '/':
              answer = answer / Number(value);
              break;

            default:
              break;
          }
        }
      });
    }

    return String(answer);
  }

  constructor() {}

  ngOnInit(): void {}
}
